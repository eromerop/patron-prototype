package factorys;

import abstracts.Calentador;
import abstracts.Lampara;
import herramientas.CalentadorSolar;
import herramientas.LamparaSolar;

public class FabricaHerramientasSolar implements FabricaHerramientas {


    @Override
    public Lampara createLampara(String tamano, String pilas, int nivel){


        return new LamparaSolar(tamano, pilas, nivel);
    }
    @Override
    public Calentador createCalentador(String modelo, String capacidad, int tubos ){

        return new CalentadorSolar(modelo, capacidad, tubos);
    }

}
